#!/usr/bin/python2.7

import re
import pprint
from datetime import datetime, timedelta
import sys, getopt
import json
import os

def arguments(argv):
   help_info = ' -i <intervalInMins> -l <logfile> -d <instanceid>'
   try:
      opts, args = getopt.getopt(argv,"hi:l:d:")
   except getopt.GetoptError:
      print help_info
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print help_info
         sys.exit()
      elif opt in ("-i"):
         interval_minutes = arg
      elif opt in ("-l"):
         log_file = arg
      elif opt in ("-d"):
         instance_id = arg

   return {"interval_minutes" : interval_minutes, "log_file" : log_file, "instance_id" : instance_id}

def log_line_translate(line):
    regex = r'(\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b) .* (\[.*\]) \"(\w{1,4}) (.*)( HTTP\/1\.1)\" ([0-9]{3})(?: )([0-9]*) "([^"]*)" "([^"]*)"'
    regex_compile = re.compile(regex, re.IGNORECASE)
    regex_match = regex_compile.match(line)
    line_Dict = dict()

    if regex_match:
        if regex_match.group(1):
            line_Dict['requestor_IP'] = regex_match.group(1)
        if regex_match.group(2):
            date = regex_match.group(2)
            date = date.replace("[", "")
            date = date.replace("]", "")
            date = date.split(" ",1)
            line_timestamp = datetime.strptime(date[0], "%d/%b/%Y:%H:%M:%S")
            line_Dict['date_time'] = line_timestamp
        if regex_match.group(3):
            line_Dict['request_type'] = regex_match.group(3)
        if regex_match.group(4):
            line_Dict['request_route'] = regex_match.group(4)
        if regex_match.group(5):
            line_Dict['HTTP_version'] = regex_match.group(5)
        if regex_match.group(6):
            line_Dict['status_code'] = regex_match.group(6)
        if regex_match.group(7):
            line_Dict['response_size'] = regex_match.group(7)
        if regex_match.group(8):
            line_Dict['referer'] = regex_match.group(8)
        if regex_match.group(9):
            line_Dict['user_agent'] = regex_match.group(9)

        return line_Dict

#get log file
text_file = open(arguments(sys.argv[1:])["log_file"], "r")
log_lines = text_file.read().split('\n')
log_list = []

#define interval
interval_minutes = arguments(sys.argv[1:])["interval_minutes"]
current_time = datetime.now()
comparison_time = current_time - timedelta(minutes=int(interval_minutes))

#get instance id from arguments
instance_id = arguments(sys.argv[1:])["instance_id"]

#convert log to json data
json_data = []

for line in log_lines:
    line_translated = log_line_translate(line)
    if line_translated:
        if line_translated['date_time'] > comparison_time:
            os.system("aws cloudwatch put-metric-data --metric-name 'HTTP Requests' --dimensions InstanceId=$(ec2metadata --instance-id) --namespace 'EC2' --value 1 --timestamp '"+line_translated['date_time'].isoformat(' ')+"'")